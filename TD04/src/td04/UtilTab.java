/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td04;

/**
 *
 * @author Diane
 */
public class UtilTab {
    
    public static double somme(double ... valeurs)
    {
        double s=0;
        for(double argi : valeurs)
            s=s+argi;
        return s;
    }
    
    public static double moyenne(double ... valeurs)
    {
        if(!tabVide(valeurs))
            return UtilTab.somme(valeurs)/valeurs.length;
        else
            return 0;
    }
    
    public static double min(double ... valeurs)
    {
        if(!tabVide(valeurs))
        {
            double mini = valeurs[0];
            for (double argi : valeurs)
                if(argi<mini)
                    mini=argi;
            return mini;
        }
        else
            return 0;
    }
    
    public static double max(double ... valeurs)
    {
        if(!tabVide(valeurs))
        {
            double maxi = valeurs[0];
            for (double argi : valeurs)
                if(argi>maxi)
                    maxi=argi;
            return maxi;
        }
        else
            return 0;
    }
    
    public static void copie (double tab1[],double tab2[]){
        /*
            créer une nouveau tableau dans lequel sont recopiées les valeurs
            du tableau mis en argument
         */

        for(int i=0; i<tab1.length && i<tab2.length; i++) tab2[i]=tab1[i];

    }
    
    public static double[] genereTabAlea(int nbCases)
    {
        double tab[] = new double[nbCases];
        for(int i=0;i<nbCases;i++)
            tab[i]=Math.random();
        return tab;
            
    }
    
    public static void affichage(double ... valeurs)
    {
        if(!tabVide(valeurs))
        {
            for(double argi : valeurs)
                System.out.printf("%f\t\t", argi);
            System.out.print("\n");
        }
        else
            System.out.println("Le tableau est vide.");
    }
    
    private static boolean tabVide(double tab[]) {

        if (tab.length==0 || tab==null) return true;
        else return false;

    }
    
    public static float somme(float tab[][])
    {
        float somme=0f;
        for(int i=0;i<tab.length;i++)
            for(int j=0;j<tab[i].length;j++)
                somme=somme+tab[i][j];
        return somme;
    }

    public static float moyenne(float tab[][])
    {
        if(!tabVide(tab))
        {
            int longueur=0;
            for(int i=0;i<tab.length;i++)
                longueur=longueur + tab[i].length;
            return UtilTab.somme(tab)/longueur;
        }
        else
            return 0;
    }
    
    public static float sommeLigne(float tab[][], int indiceL)
    {
        float somme=0;
        if(indiceL>= 0 && indiceL<tab.length)
        {
            for(int i=0;i<tab[indiceL].length;i++)
                somme=somme+tab[indiceL][i];
        }
        else
            System.out.println("Cette ligne n'existe pas.");
        return somme;
    }
    
    public static float sommeColonne(float tab[][],int indiceC)
    {
        float somme=0;
        if(indiceC>=0)
        {
            for(int i=0;i<tab.length;i++)
            {
                if(indiceC<tab[i].length)
                    somme=somme+tab[i][indiceC]; 
            }
        }
        else
            System.out.println("Cette colonne n'existe pas.");
            
        return somme;
    }
    
    public static void affichage(float tab[][])
    {
        for (int i=0;i<tab.length;i++)
        {
            for(int j=0;j<tab[i].length;j++)
                System.out.printf("%f\t\t", tab[i][j]);
            System.out.print("\n");
        }
    }
    
    public static void copie (float tab1[][], float tab2[][]) {

        /*
            créer une nouveau tableau dans lequel sont recopiées les valeurs
            du tableau mis en argument
            i lignes de tab2
            j colonnes de tab2
         */

        int i,j;
        i=j=0;

        for (int k=0 ; k<tab1.length && i<tab2.length; k++)
           for (int l=0; l<tab1[k].length; l++) {

               if (j < tab2[i].length) {
                   tab2[i][j] = tab1[k][l];
                   j++;
                   
               } else {
                   j=0;
                   i++;
                   tab2[i][j] = tab1[k][l];
               }
           }
    }
    
    private static boolean tabVide(float tab[][]) {

        if (tab.length==0) return true;
        else return false;

    }
}
