/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.twoDim;

import geometry.oneDim.Segment2D;

/**
 *
 * @author Diane
 */
public class Triangle2D {
    public Triangle2D(Segment2D s1, Segment2D s2, Segment2D s3)
    {
        seg1=s1;
        seg2=s2;
        seg3=s3;
    }
    
    public void affiche() {
        System.out.println("Je suis un triangle compose des segments suivants : ");
        seg1.affiche();
        seg2.affiche();
        seg3.affiche();
    }
    
    public float perimetre() {
        return seg1.longeur()+seg2.longeur()+seg3.longeur();
    }
    
    public float aire() {
        float s;
        s=this.perimetre()/2f;
        return (float)Math.sqrt(s*(s-seg1.longeur())*(s-seg2.longeur())*(s-seg3.longeur()));
    }
    
    private Segment2D seg1;
    private Segment2D seg2;
    private Segment2D seg3;    
}
