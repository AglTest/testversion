/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.twoDim;

import geometry.oneDim.Segment2D;

/**
 *
 * @author Diane
 */
public class Rectangle2D {
    public Rectangle2D(Segment2D s1,Segment2D s2,Segment2D s3,Segment2D s4)
    {
        seg1=s1;
        seg2=s2;
        seg3=s3;
        seg4=s4;
    }
    
    public void affiche() {
        System.out.println("Je suis un rectangle compose des segments suivants : ");
        seg1.affiche();
        seg2.affiche();
        seg3.affiche();
        seg4.affiche();
    }
    
    public float perimetre() {
        return seg1.longeur()+seg2.longeur()+seg3.longeur() + seg4.longeur();
    }
    
    public float aire() {
        if(seg1.longeur()!=seg2.longeur())
            return seg1.longeur()*seg2.longeur();
        else
            return seg1.longeur()*seg3.longeur();
    }
    
    private Segment2D seg1;
    private Segment2D seg2;
    private Segment2D seg3;
    private Segment2D seg4;    
}
