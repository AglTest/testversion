/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.zeroDim;

/**
 *
 * @author Diane
 */
public class Point2D {
    public Point2D(float x, float y)
    {
        this.x=x;
        this.y=y;
    }
    
    public Point2D()
    {
        Point2D p = new Point2D(0,0);
        this.x=p.getX();
        this.y=p.getY();
    }
    
    public Point2D(float x)
    {
        Point2D p = new Point2D(x,0);
        this.x=p.getX();
        this.y=p.getY();
    }
    
    public void deplace(float dx, float dy)
    {
        x+=dx;
        y+=dy;
    }
    
    public void affiche()
    {
        System.out.println("coord = " + x + " " + y);
    }
    
    public float getX() {
        return this.x;
    }
    
    public float getY() {
        return this.y;
    }
    
    public void setX(float unX) {
        x=unX;
    }
    
    public void setY(float unY) {
        y=unY;
    }
    
    public float distance(Point2D p)
    {
        return (float)Math.sqrt(Math.pow(p.getY()-this.getY(),2) + Math.pow(this.getX()-p.getX(),2));
    }
    
    //Sens de rotation trigonométrique : vers la gauche
    public Point2D rotation(double angleEnRadians)
    {
        Point2D p = new Point2D();
        double angle1=Math.asin(this.getY()/this.distance(p));
        System.out.println(angle1);
        double angle2=angle1+angleEnRadians;
        System.out.println(angle2);
        float abs=(float)Math.cos((float)angle2)*this.distance(p);
        float ord=(float)Math.sin((float)angle2)*this.distance(p);
        Point2D p2 = new Point2D(abs,ord);
        return p2;
    }
    private float x,y; 
}
