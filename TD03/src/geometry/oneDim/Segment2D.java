/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.oneDim;

import geometry.zeroDim.Point2D;

/**
 *
 * @author Diane
 */
public class Segment2D {
    public Segment2D (Point2D p1, Point2D p2) {
        ext1=p1;
        ext2=p2;
    }
    
    public float longeur() {
        return (float)Math.sqrt(Math.pow(ext2.getY()-ext1.getY(),2) + Math.pow(ext1.getX()-ext2.getX(),2));
    }
    
    public void affiche() {
        System.out.println("Je suis un segment d'extremites : ");
        ext1.affiche();
        ext2.affiche();
    }
    
    public void deplaceP1(float dxP1, float dyP1) {
        ext1.setX(ext1.getX()+dxP1);
        ext1.setY(ext1.getY()+dyP1);
    }
    
    public void deplaceP2(float dxP2, float dyP2) {
        ext2.setX(ext2.getX()+dxP2);
        ext2.setY(ext2.getY()+dyP2);
    }
    
    private Point2D ext1;
    private Point2D ext2;    
}
