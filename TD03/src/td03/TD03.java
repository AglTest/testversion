/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td03;

/**
 *
 * @author Diane
 */
public class TD03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Compteur c1 = new Compteur();
        Compteur c2 = new Compteur();
        Compteur c3 = new Compteur();
        
        System.out.println(c1.getId());
        System.out.println(c2.getId());        
        System.out.println(c3.getId());
        System.out.println(Compteur.getIdMax());        
        Compteur c4 = new Compteur();        
        System.out.println(c4.getId());        
        System.out.println(Compteur.getIdMax());
    }
    
}
