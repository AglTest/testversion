/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td03;

/**
 *
 * @author Diane
 */
public class Compteur {
    private static int id;
    
    static {
        do
        {
            System.out.println("Entrez l'id de depart");
            id=Clavier.lireInt();
        }while(id<0);
    }
    
    public Compteur() {
      id2=id;
      id++;
    }
    public int getId() {
        return id2;
    }
    
    public static int getIdMax() {
        return id-1;
    }
    
    private int id2;
}
