/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.Triangle2D;

import geometry.zeroDim.Point2D;
import geometry.oneDim.Segment2D;
import geometry.twoDim.Triangle2D;

/**
 *
 * @author Diane
 */
public class TestsTriangle2D {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(1,1);
        Point2D p2 = new Point2D(3,1);
        Segment2D seg1_2 = new Segment2D(p1,p2);
        Point2D p3 = new Point2D(3,2);
        Segment2D seg2_3 = new Segment2D(p2,p3);
        Segment2D seg3_1 = new Segment2D(p3,p1);
        Triangle2D tri = new Triangle2D(seg1_2,seg2_3,seg3_1);
        tri.affiche();
        System.out.println("Perimetre = " + tri.perimetre());
        System.out.println("Aire = " + tri.aire());
    }
}