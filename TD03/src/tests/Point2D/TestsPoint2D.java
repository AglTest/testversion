/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.Point2D;
import geometry.zeroDim.*;
/**
 *
 * @author Diane
 */
public class TestsPoint2D {
    public static void main(String[] args) {
        Point2D p1 =new Point2D(1,1);
        p1.affiche();
        p1.deplace(1, 0);
        p1.affiche();
        
        Point2D p2 = new Point2D(3,1);
        System.out.println(p1.distance(p2));
        
        Point2D p3 = new Point2D();
        p3.affiche();
        
        Point2D p4 = new Point2D(1,1);
        p4.affiche();
        
        Point2D p5 = new Point2D();
        p5=p4.rotation(3.14/2);
        p5.affiche();
    }
}
