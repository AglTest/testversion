/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.Rectangle2D;

import geometry.zeroDim.Point2D;
import geometry.twoDim.Rectangle2D;
import geometry.oneDim.Segment2D;

/**
 *
 * @author Diane
 */
public class TestsRectangle2D {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(1,1);
        Point2D p2 = new Point2D(3,1);
        Segment2D seg1_2 = new Segment2D(p1,p2);
        Point2D p3 = new Point2D(3,2);
        Point2D p4 = new Point2D(1,2);
        Segment2D seg2_3 = new Segment2D(p2,p3);
        Segment2D seg3_4 = new Segment2D(p3,p4);
        Segment2D seg4_1 = new Segment2D(p4,p1);
        Rectangle2D rect = new Rectangle2D(seg1_2,seg2_3,seg3_4,seg4_1);
        rect.affiche();
        System.out.println("Perimetre = " + rect.perimetre());
        System.out.println("Aire = " + rect.aire());
    }
}