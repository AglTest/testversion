/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.Segment2D;

import geometry.zeroDim.Point2D;
import geometry.oneDim.Segment2D;

/**
 *
 * @author Diane
 */
public class TestsSegment2D {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(1,1);
        Point2D p2 = new Point2D(3,1);
        Segment2D seg = new Segment2D(p1,p2);
        System.out.println("longueur = " + seg.longeur());
        seg.affiche();
        seg.deplaceP1(-1, 0);
        seg.deplaceP2(1,0);
        System.out.println("longueur = " + seg.longeur());
        seg.affiche();
    }
}
