/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td01;

/**
 *
 * @author Diane
 */
public class Exercice4 {
    public static char affichage(int i, int j, int k, int l, int m)
    {
        System.out.println(i+"pieces de 1 euro.");
        System.out.println(j+"pieces de 50 centimes.");
        System.out.println(k+"pieces de 20 centimes.");
        System.out.println(l+"pieces de 10 centimes.");
        System.out.println(m+"pieces de 5 centimes.");
        System.out.println("Voulez-vous continuer ? o/n");
        char rep=Clavier.lireChar();
        return(rep);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("Donnez un montant :");
        int montant_initial=(Clavier.lireInt())*100;
        
        int i=0;
        int j=0;
        int k=0;
        int l=0;
        int m=0;
        int total_1,total_50,total_20,total_10,total_5;
        int montant_1,montant_50,montant_20,montant_10,montant_5;
        total_1=(int)montant_initial/100;
        etiq : for(i=0;i<=total_1;i++)
        {
            j=0;
            k=0;
            l=0;
            m=0;
            montant_1=montant_initial-100*i;
            total_50=(int)montant_1/50;
            for(j=0;j<=total_50;j++)
            {
                k=0;
                l=0;
                m=0;
                montant_50=montant_1-50*j;
                total_20=(int)montant_50/20;
                for(k=0;k<=total_20;k++)
                {
                    l=0;
                    m=0;
                    montant_20=montant_50-20*k;
                    total_10=(int)montant_20/10;
                    for(l=0;l<=total_10;l++)
                    {
                        m=0;
                        montant_10=montant_20-10*l;
                        total_5=(int)montant_10/5;
                        for(m=0;m<=total_5;m++)
                        {
                            montant_5=montant_10-5*m;
                            if(montant_5==0)
                                {
                                    if(affichage(i,j,k,l,m)=='n') break etiq; 
                                }
                        }
                    }
                }
            }
        }
    } 
}
