/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td02bis;

/**
 *
 * @author Diane
 */
public class TD02Bis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //On définit le nombre de voyages
        final int taille=3;
        
        //Les voyages saisis seront dans un tableau
        Voyage[] tableau ; 
        tableau = new Voyage[taille];
        
        //Saisie des voyages de l'agence de voyage
        System.out.println("Veuillez saisir "+taille+" voyages :");
        for(int i=0;i<taille;i++)
        {
            System.out.println("Voyage numero "+ (i+1));
            Voyage v = new Voyage();
            v.saisirLesVoyages();
            tableau[i] = v;
        }
        
        //Affichage des voyages saisis
        System.out.println("Voici les voyages que vous avez saisis :");
        for (int i=0;i<taille;i++)
        {
            tableau[i].affiche();
        }
        
        //Saisie d'une ville de départ et d'arrivée par le client
        System.out.println("Veuillez saisir une ville de départ et d'arrivée en tant que client :");
        Voyage v1=new Voyage();
        v1.setVilleDepart();
        v1.setVilleArrivee();
        v1.affiche();
        
        /* Tests
        System.out.println(v1.getDepart());
        System.out.println(v1.getArrivee());
        System.out.println(tableau[0].getDepart());
        System.out.println(tableau[0].getArrivee());
        */
        
        //Création d'un tableau tab récupérant les indices des voyages de tableau correspondant à la recherche
        int j=0;
        int tab[];
        tab= new int[taille];
        //initialisation du tableau de int à 0;
        for(int i=0;i<taille;i++)
            {tab[i]=0;}

        Voyage lepluscourt = new Voyage();
        Voyage lemoinscher = new Voyage();

        for(int i=0;i<taille;i++) 
        {
            if(v1.getDepart().equals(tableau[i].getDepart()) && v1.getArrivee().equals(tableau[i].getArrivee())) 
            {
                tab[j]= i;
                j++;
            }
        }  
        
        //pour les objets séléctionnées je garde que les objets les moins chers et les plus courts
        //1- Cas où aucun voyage ne correspond
        if(j==0)   System.out.println("Il n'y a pas de voyage correspondant:"); 

        else
        {
            //Initialisation des variables
            lepluscourt=tableau[tab[0]];
            lemoinscher=tableau[tab[0]];
            
            //2- S'il n'y a qu'un seul élément trouvé on optimise en affichant le voyage, qui sera forcément le plus court et le moins cher
            //3-Le moins cher et le plus court est le même voyage
            if (j==1 || lepluscourt.equals(lemoinscher))  
            {
                System.out.println("Le voyage le moins cher et le plus court disponible:"); 
                lepluscourt.affiche();
            }
            else
            {    
                for(j=1;j<tab.length;j++)
                {
                    lepluscourt=lepluscourt.comparerLesMemesVoyages(tableau[tab[j]]);
                    lemoinscher=lemoinscher.comparerLesPrixSurMemesVoyages(tableau[tab[j]]);
                }
                //affichage du résultat
                //4- Le moins cher et le plus court sont des voyages différents
                System.out.println("Le voyage le plus court disponible:"); 
                lepluscourt.affiche();              
                System.out.println("Le voyage le moins cher disponible:"); 
                lemoinscher.affiche();     
            }
        }
    }    
}
