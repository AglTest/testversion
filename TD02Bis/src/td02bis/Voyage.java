/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td02bis;

/**
 *
 * @author Diane
 */
public class Voyage {
        
     
    public Voyage ()//constructeur par défaut
    {
        prix = new ReelContraint(0.05,3.2);
        longueur = new ReelContraint(1,30000);
        villedep=new String();
        villearr=new String();
    }

    public Voyage (String uneVilleDepart, String uneVilleArrivee)//constructeur par défaut
    {
        ReelContraint unprix = new ReelContraint(0.05,3.2);
        ReelContraint unelongueur = new ReelContraint(1,30000);

        villedep=uneVilleDepart;
        villearr=uneVilleArrivee;
        prix=unprix;
        longueur=unelongueur;
    }

    public Voyage (String uneVilleDepart, String uneVilleArrivee, double pPrix, double pLongueur )//constructeur ensemble des paramètres
    {
        prix = new ReelContraint(0.05,3.2,pPrix);
        longueur = new ReelContraint(1,30000,pLongueur);

        villedep=uneVilleDepart;
        villearr=uneVilleArrivee;
    }

    public void saisirLesVoyages(String uneVilleDeP, String uneVilleArr, double unPrix, double uneLongueur)//a appeler si le constructeur par défault est appelé
    {
        setVilleDepart(uneVilleDeP);
        setVilleArrivee(uneVilleArr);
        setPrix(unPrix);
        setLongueur(uneLongueur);
    }
    
    //Ou pas d'argument, pas static et on met this ou static avec argument
    public static double calculPrix(Voyage v)
    {
        return v.getLongueur()*v.getPrix();
    }
    
    public void affiche()
    {
        System.out.println("Ville de depart = " + getDepart() + " Ville d'arrivée= "+ getArrivee()+ " Longueur du voyage "+ getLongueur()+" Le prix "+ getPrix());
    }

    public Voyage comparerLesMemesVoyages(Voyage voyage)
    {
        Voyage lepluscourt= new Voyage();
        
        if(longueur.getValReel() > voyage.getLongueur())
        {
            lepluscourt=voyage;
        }
        else 
        {  
            lepluscourt=this;
        }
        return(lepluscourt); 
    }

    public Voyage comparerLesPrixSurMemesVoyages(Voyage voyage)
    {
        Voyage lemoinscher= new Voyage();

        if(calculPrix(this) < calculPrix(voyage)) 
        {    
            lemoinscher=this;
        }

        else 
        {
            lemoinscher=voyage;
        }
        return(lemoinscher);
    }

    public double getLongueur()
    {
        return (longueur.getValReel());
    }

    public double getPrix()
    {
        return (prix.getValReel());
    }
    
    public String getDepart()
    {
        return (villedep);
    }
    
    public String getArrivee()
    {
        return (villearr);
    }

    public void setPrix(double unPrix)
    { 
       prix.setValReel(unPrix);
    }

    public void setLongueur(double uneLongueur)
    { 
        longueur.setValReel(uneLongueur);
    }
    
    //ATTENTION pas d'interaction avec l'utilisateur
    public void setVilleDepart(String uneVille)//a appeler si le constructeur par défaut est appelé
    {
        villedep=uneVille;
    }

    public void setVilleArrivee(String uneVille)//a appeler si le constructeur par défaut est appelé
    {
        villearr=uneVille;
    }

    //ATTENTION : tout mettre en final ou rien mettre en final
    private ReelContraint longueur;
    private ReelContraint prix; 
    private String villedep;
    private String villearr;
}
