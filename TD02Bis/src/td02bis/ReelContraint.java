/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td02bis;

/**
 *
 * @author Diane
 */
public class ReelContraint {
    
    public ReelContraint () //constructeur
    {
        double unMin=0, unMax=0,uneVal=0;
        
        if(unMin<=unMax)
        {   
            min=unMin;
            max=unMax;
        }
        else
        {   
            min=unMax;
            max=unMin;
        }
    }
      
    public ReelContraint (double unMin, double unMax) //constructeur
    {
               
        if(unMin<=unMax)
        {   
            min=unMin;
            max=unMax;
        }
        else
        {   
            min=unMax;
            max=unMin;
        }
        
    }
    
     public ReelContraint (double unMin, double unMax, double uneVal) //constructeur
    {
               
        if(unMin<=unMax)
        {   
            min=unMin;
            max=unMax;
        }
        else
        {   
            min=unMax;
            max=unMin;
        }
        
        
        if( min<= uneVal && uneVal <=max)
            val = uneVal ;
        else
        {
            if(uneVal>max)
                val = max ;
            else
                val = min ;
        }
    }
    
    public void affichageReelContraint()
    {
        System.out.println("val = " + val+ "dans une intervalle ["+ min+",] ");
    }
   
   public void saisirReelContraint()
   {
        double uneVal;
       
        do{
        System.out.println("Saisir une valeur comprise entre "+ min + " et " + max);
        uneVal=Clavier.lireDouble();
        }while ( uneVal < min || uneVal > max);
      
        val=uneVal; 
   }   
    
    public double getMinReel()
    {  
        return (min);
    }
    
    public double getMaxReel()
    {
        return (max);
    }
    
    public double getValReel()
    {
        return(val);
    }
    
    public void setValReel(double uneVal)
    { 
        if (uneVal>min && uneVal<max) 
        {val=uneVal;
        }
    }
    
   //paramètres
   final private double min;
   final private double max;
   private double val;
    
    
}
